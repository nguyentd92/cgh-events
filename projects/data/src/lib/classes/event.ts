import { User } from './user';

export class CghEvent {
  id: number;
  title: string;
  description: string;
  logo: string;
  price: number;
  totalSeats: number;
  address: string;
  city: string;
  province: string;
  country: string;
  startDate: string;
  startTime: string;
  endDate: string;
  endTime: string;
  status: EventStatus;
  completion?: number;
  introduction?: string;
  performers?: User[];
  blClass?: string;
}

export type EventStatus = 'pending' | 'completed' | 'on scheduled' | 'delay';

const MOCK_PERFORMERS: User[] = [
  {
    displayName: 'john',
    avatarUrl: 'assets/img/theme/team-1-800x800.jpg'
  },
  {
    displayName: 'john',
    avatarUrl: 'assets/img/theme/team-2-800x800.jpg'
  },
  {
    displayName: 'john',
    avatarUrl: 'assets/img/theme/team-3-800x800.jpg'
  },
  {
    displayName: 'john',
    avatarUrl: 'assets/img/theme/team-4-800x800.jpg'
  }
];

export const enum EVENT_STATUS {
  PENDING = 'pending',
  COMPLETED = 'completed',
  ONSCHEDULE = 'on scheduled',
  DELAYED = 'delay'
}

export const EVENTS: CghEvent[] = [
  {
    id: 1,
    title: 'Argon Design System',
    logo: 'assets/img/theme/bootstrap.jpg',
    description: '',
    totalSeats: 146989754,
    price: 2500,
    status: EVENT_STATUS.DELAYED,
    performers: MOCK_PERFORMERS,
    completion: 0.5,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 2,
    title: 'Angular X',
    logo: 'assets/img/theme/angular.jpg',
    description: '',
    totalSeats: 36624199,
    price: 2500,
    status: EVENT_STATUS.ONSCHEDULE,
    performers: MOCK_PERFORMERS,
    completion: 0.7,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 3,
    title: 'Sketch',
    logo: 'assets/img/theme/sketch.jpg',
    description: '',
    totalSeats: 324459463,
    price: 3000,
    status: EVENT_STATUS.COMPLETED,
    performers: MOCK_PERFORMERS,
    completion: 0.8,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 4,
    title: 'ReactJS',
    logo: 'assets/img/theme/react.jpg',
    description: '',
    totalSeats: 1409517397,
    price: 3000,
    status: EVENT_STATUS.PENDING,
    performers: MOCK_PERFORMERS,
    completion: 0.9,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 5,
    title: 'Argon Design System',
    logo: 'assets/img/theme/bootstrap.jpg',
    description: '',
    totalSeats: 146989754,
    price: 2500,
    status: EVENT_STATUS.DELAYED,
    performers: MOCK_PERFORMERS,
    completion: 0.5,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 6,
    title: 'Angular X',
    logo: 'assets/img/theme/angular.jpg',
    description: '',
    totalSeats: 36624199,
    price: 2500,
    status: EVENT_STATUS.ONSCHEDULE,
    performers: MOCK_PERFORMERS,
    completion: 0.7,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 7,
    title: 'Sketch',
    logo: 'assets/img/theme/sketch.jpg',
    description: '',
    totalSeats: 324459463,
    price: 3000,
    status: EVENT_STATUS.COMPLETED,
    performers: MOCK_PERFORMERS,
    completion: 0.8,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 8,
    title: 'ReactJS',
    logo: 'assets/img/theme/react.jpg',
    description: '',
    totalSeats: 1409517397,
    price: 3000,
    status: EVENT_STATUS.PENDING,
    performers: MOCK_PERFORMERS,
    completion: 0.9,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 9,
    title: 'Vue Paper UI Kit PRO',
    logo: 'assets/img/theme/vue.jpg',
    description: '',
    totalSeats: 1409517397,
    price: 2200,
    status: EVENT_STATUS.PENDING,
    performers: MOCK_PERFORMERS,
    completion: 0.3,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 10,
    title: 'Vue Paper UI Kit PRO',
    logo: 'assets/img/theme/vue.jpg',
    description: '',
    totalSeats: 1409517397,
    price: 2200,
    status: EVENT_STATUS.PENDING,
    performers: MOCK_PERFORMERS,
    completion: 0.3,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 11,
    title: 'Argon Design System',
    logo: 'assets/img/theme/bootstrap.jpg',
    description: '',
    totalSeats: 146989754,
    price: 2500,
    status: EVENT_STATUS.DELAYED,
    performers: MOCK_PERFORMERS,
    completion: 0.5,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 12,
    title: 'Angular X',
    logo: 'assets/img/theme/angular.jpg',
    description: '',
    totalSeats: 36624199,
    price: 2500,
    status: EVENT_STATUS.ONSCHEDULE,
    performers: MOCK_PERFORMERS,
    completion: 0.7,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 13,
    title: 'Sketch',
    logo: 'assets/img/theme/sketch.jpg',
    description: '',
    totalSeats: 324459463,
    price: 3000,
    status: EVENT_STATUS.COMPLETED,
    performers: MOCK_PERFORMERS,
    completion: 0.8,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 14,
    title: 'ReactJS',
    logo: 'assets/img/theme/react.jpg',
    description: '',
    totalSeats: 1409517397,
    price: 3000,
    status: EVENT_STATUS.PENDING,
    performers: MOCK_PERFORMERS,
    completion: 0.9,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  },
  {
    id: 15,
    title: 'Argon Design System',
    logo: 'assets/img/theme/bootstrap.jpg',
    description: '',
    totalSeats: 146989754,
    price: 2500,
    status: EVENT_STATUS.DELAYED,
    performers: MOCK_PERFORMERS,
    completion: 0.5,
    address: '28 Nguyen Tri Phuong',
    city: 'Hue',
    province: 'Thua Thien Hue',
    country: 'Vietnam',
    startDate: '01/03/2019',
    startTime: '08:00',
    endDate: '01/03/2019',
    endTime: '11:00'
  }
];

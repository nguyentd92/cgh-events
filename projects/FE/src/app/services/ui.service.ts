import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class UiService {
  private scrolledSubject = new BehaviorSubject<boolean>(false)
  constructor() { }

  get scrolled$(): Observable<boolean> {
    return this.scrolledSubject.asObservable();
  }

  checkScrolled(offsetTop: number) {
    if(!this.scrolledSubject.value && offsetTop > 0) {
      this.scrolledSubject.next(true)
    } else if(this.scrolledSubject.value && offsetTop == 0) {
      this.scrolledSubject.next(false)
    }
  }
}

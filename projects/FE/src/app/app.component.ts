import { Component, AfterViewInit, Host, HostListener } from "@angular/core";
import { UiService } from "./services/ui.service";
import { NavigationEnd, Router } from "@angular/router";
import { NgwWowService } from "ngx-wow";
import { filter } from "rxjs/operators";

declare var $;

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements AfterViewInit {
  title = "FE";

  constructor(
    public uiSv: UiService,
    private router: Router,
    private wowService: NgwWowService
  ) {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(event => {
        // Reload WoW animations when done navigating to page,
        // but you are free to call it whenever/wherever you like
        this.wowService.init();
      });
  }

  @HostListener("window:scroll", ["$event"])
  onScroll() {
    this.uiSv.checkScrolled(window.pageYOffset);
  }

  ngAfterViewInit() {
    $(document).ready(function() {
      console.log("Print : ");
      // confer.loadAll();
      // active.active();

      // Avoid `console` errors in browsers that lack a console.
      (function() {
        var method;
        var noop = function noop() {};
        var methods = [
          "assert",
          "clear",
          "count",
          "debug",
          "dir",
          "dirxml",
          "error",
          "exception",
          "group",
          "groupCollapsed",
          "groupEnd",
          "info",
          "log",
          "markTimeline",
          "profile",
          "profileEnd",
          "table",
          "time",
          "timeEnd",
          "timeStamp",
          "trace",
          "warn"
        ];
        var length = methods.length;
        var console = window.console || {};
        while (length--) {
          method = methods[length];
          // Only stub undefined methods.
          if (!console[method]) {
            console[method] = noop;
          }
        }
      })();

      // **********************************************
      // ** Classy Nav - 1.1.0
      // ** Responsive Megamenu Plugins
      // ** Copyright (c) 2019 Designing World
      // **********************************************

      (function($) {
        console.log("hello");
        const fn: any = $.fn;
      })(jQuery);
    });

    this.active();
  }

  active() {
    "use strict";

    console.log("call active");

    var confer_window = $(window);

    // ****************************
    // :: 1.0 Preloader Active Code
    // ****************************

    confer_window.on("load", function() {
      console.log("fade");
      $("#preloader").fadeOut("1000", function() {
        $(this).remove();
      });
    });

    // ******************************
    // :: 17.0 ScrollDown Active Code
    // ******************************

    $("#scrollDown").on("click", function() {
      $("html, body").animate(
        {
          scrollTop: $("#about").offset().top - 75
        },
        800
      );
    });

    // ************************************
    // :: 4.0 Instragram Slides Active Code
    // ************************************

    if ($.fn.owlCarousel) {
      var clientArea = $(".client-area");
      clientArea.owlCarousel({
        items: 2,
        loop: true,
        autoplay: true,
        smartSpeed: 1000,
        margin: 40,
        autoplayTimeout: 7000,
        nav: true,
        navText: [
          '<i class="zmdi zmdi-chevron-left"></i>',
          '<i class="zmdi zmdi-chevron-right"></i>'
        ],
        responsive: {
          0: {
            items: 1
          },
          576: {
            items: 2,
            margin: 15
          },
          992: {
            margin: 20
          },
          1200: {
            margin: 40
          }
        }
      });
    }

    // *********************************
    // :: 5.0 Masonary Gallery Active Code
    // *********************************

    if ($.fn.imagesLoaded) {
      $(".confer-portfolio").imagesLoaded(function() {
        // filter items on button click
        $(".portfolio-menu").on("click", "button", function() {
          var filterValue = $(this).attr("data-filter");
          $grid.isotope({
            filter: filterValue
          });
        });
        // init Isotope
        var $grid = $(".confer-portfolio").isotope({
          itemSelector: ".single_gallery_item",
          percentPosition: true,
          masonry: {
            columnWidth: ".single_gallery_item"
          }
        });
      });
    }

    // ***********************************
    // :: 6.0 Counter Up Active Code
    // ***********************************
    if ($.fn.counterUp) {
      $(".counter").counterUp({
        delay: 10,
        time: 2000
      });
    }

    // ***********************************
    // :: 6.0 Portfolio Button Active Code
    // ***********************************

    $(".portfolio-menu button.btn").on("click", function() {
      $(".portfolio-menu button.btn").removeClass("active");
      $(this).addClass("active");
    });

    // ********************************
    // :: 7.0 Search Button Active Code
    // ********************************
    $(".search-btn").on("click", function() {
      $(".search-form").toggleClass("search-form-active");
    });

    // *********************************
    // :: 9.0 Magnific Popup Active Code
    // *********************************
    if ($.fn.magnificPopup) {
      $(".video-play-btn").magnificPopup({
        type: "iframe"
      });
      $(".portfolio-img").magnificPopup({
        type: "image",
        gallery: {
          enabled: true,
          preload: [0, 2],
          navigateByImgClick: true,
          tPrev: "Previous",
          tNext: "Next"
        }
      });
      $(".single-gallery-item").magnificPopup({
        type: "image",
        gallery: {
          enabled: true,
          preload: [0, 2],
          navigateByImgClick: true,
          tPrev: "Previous",
          tNext: "Next"
        }
      });
    }

    // **************************
    // :: 10.0 Tooltip Active Code
    // **************************
    if ($.fn.tooltip) {
      $('[data-toggle="tooltip"]').tooltip();
    }

    // ****************************
    // :: 12.0 Jarallax Active Code
    // ****************************
    if ($.fn.jarallax) {
      $(".jarallax").jarallax({
        speed: 0.5
      });
    }

    // ****************************
    // :: 13.0 Countdown Active Code
    // ****************************
    if ($.fn.countdown) {
      $("#clock").countdown("2019/08/29", function(event) {
        $(this).html(
          event.strftime(
            "<div>%m <span>Months</span></div> <div>%d <span>Days</span></div> <div>%H <span>Hours</span></div> <div>%M <span>Minutes</span></div> <div>%S <span>Seconds</span></div>"
          )
        );
      });
    }

    // ****************************
    // :: 13.0 Scrollup Active Code
    // ****************************
    if ($.fn.scrollUp) {
      confer_window.scrollUp({
        scrollSpeed: 1000,
        scrollText: '<i class="arrow_carrot-up"</i>'
      });
    }

    // *********************************
    // :: 14.0 Prevent Default 'a' Click
    // *********************************
    $('a[href="#"]').on("click", function($) {
      $.preventDefault();
    });

    // *********************************
    // :: 14.0 Prevent Default 'a' Click
    // *********************************
    var pricingTable = $(".single-ticket-pricing-table");

    pricingTable.on("mouseenter", function() {
      pricingTable.removeClass("active");
      $(this).addClass("active");
    });
  }
}

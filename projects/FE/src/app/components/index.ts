import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SpeakerComponent } from './speaker/speaker.component';
import { NewsComponent } from './news/news.component';

export const components = [
    FooterComponent,
    HeaderComponent,
    SpeakerComponent,
    NewsComponent
]

export * from "./footer/footer.component";
export * from "./header/header.component";
export * from "./speaker/speaker.component";

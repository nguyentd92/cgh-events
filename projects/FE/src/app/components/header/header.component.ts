import { Component, OnInit, AfterViewInit, HostListener } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UiService } from '../../services/ui.service';

declare var $;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements AfterViewInit {
  // default options
  private readonly defaultOpt = {
    breakpoint: 991,
    openCloseSpeed: 500,
    megaopenCloseSpeed: 800
  };

  breakpointCheckSubject: BehaviorSubject<boolean>;
  breakpointCheck$: Observable<boolean>;
  constructor(public uiSv: UiService) {
    this.breakpointCheckSubject = new BehaviorSubject<boolean>(
      !(window.innerWidth > this.defaultOpt.breakpoint)
    );
    this.breakpointCheck$ = this.breakpointCheckSubject.asObservable();
  }

  // Realtime listener to window resize
  @HostListener('window:resize', ['$event'])
  breakpointCheck() {
    // BreakpointCheck false if window innerwidth larger than breakpoint default, true if smaller
    if (
      this.breakpointCheckSubject.value &&
      window.innerWidth > this.defaultOpt.breakpoint
    ) {
      this.breakpointCheckSubject.next(false);
    } else if (
      !this.breakpointCheckSubject.value &&
      window.innerWidth < this.defaultOpt.breakpoint
    ) {
      this.breakpointCheckSubject.next(true);
    }
  }

  ngAfterViewInit() {
    $.fn.classyNav = function(options) {
      // Variables
      var classy_nav = $('.classynav ul');
      var classy_navli = $('.classynav > ul > li');
      var navbarToggler = $('.classy-navbar-toggler');
      var closeIcon = $('.classycloseIcon');
      var navToggler = $('.navbarToggler');
      var classyMenu = $('.classy-menu');
      var var_window = $(window);

      return this.each(function() {
        // navbar toggler
        navbarToggler.on('click', function() {
          navToggler.toggleClass('active');
          classyMenu.toggleClass('menu-on');
        });

        // close icon
        closeIcon.on('click', function() {
          classyMenu.removeClass('menu-on');
          navToggler.removeClass('active');
        });

        // add dropdown & megamenu class in parent li class
        classy_navli.has('.dropdown').addClass('cn-dropdown-item');
        classy_navli.has('.megamenu').addClass('megamenu-item');

        // adds toggle button to li items that have children
        classy_nav.find('li a').each(function() {
          if ($(this).next().length > 0) {
            $(this)
              .parent('li')
              .addClass('has-down')
              .append('<span class="dd-trigger"></span>');
          }
        });

        // expands the dropdown menu on each click
        classy_nav.find('li .dd-trigger').on('click', function(e) {
          e.preventDefault();
          $(this)
            .parent('li')
            .children('ul')
            .stop(true, true)
            .slideToggle(this.defaultOpt.openCloseSpeed);
          $(this)
            .parent('li')
            .toggleClass('active');
        });

        // add padding in dropdown & megamenu item
        $('.megamenu-item').removeClass('has-down');

        // expands the megamenu on each click
        classy_nav.find('li .dd-trigger').on('click', function(e) {
          e.preventDefault();
          $(this)
            .parent('li')
            .children('.megamenu')
            .slideToggle(this.defaultOpt.megaopenCloseSpeed);
        });

        // check browser width in real-time

        // sidebar menu enable
        if (this.defaultOpt.sideMenu === true) {
          this.navContainer
            .addClass('sidebar-menu-on')
            .removeClass('breakpoint-off');
        }
      });
    };
  }
}

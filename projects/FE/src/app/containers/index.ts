import { OurSpeakingComponent } from "./our-speaking/our-speaking.component";
import { AboutConferenceComponent } from "./about-conference/about-conference.component";
import { LatestNewsComponent } from "./latest-news/latest-news.component";
import { OfficialSponsorComponent } from "./official-sponsor/official-sponsor.component";
import { ContactUsComponent } from './contact-us/contact-us.component';
import { SchedulePlanComponent } from './schedule-plan/schedule-plan.component';
import { TicketPricingComponent } from './ticket-pricing/ticket-pricing.component';
import { WelcomeAreaComponent } from './welcome-area/welcome-area.component';
import { OurClientComponent } from './our-client/our-client.component';

export const containers = [
  OurSpeakingComponent,
  AboutConferenceComponent,
  LatestNewsComponent,
  OfficialSponsorComponent,
  ContactUsComponent,
  SchedulePlanComponent,
  TicketPricingComponent,
  WelcomeAreaComponent,
  OurClientComponent
];

export * from "./our-speaking/our-speaking.component";

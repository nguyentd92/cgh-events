import { Component, AfterViewInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-welcome-area',
  templateUrl: './welcome-area.component.html',
  styleUrls: ['./welcome-area.component.scss']
})
export class WelcomeAreaComponent implements AfterViewInit {
  customOptions: OwlOptions = {
    items: 1,
    loop: true,
    autoplay: false,
    smartSpeed: 1000,
    autoplayTimeout: 10000,
    nav: false,
    navText: [('<i class="zmdi zmdi-chevron-left"></i>'), ('<i class="zmdi zmdi-chevron-right"></i>')]
  }

  constructor() { }

  ngAfterViewInit() {
  }
}

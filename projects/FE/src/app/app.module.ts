import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

import { CarouselModule } from "ngx-owl-carousel-o";

import * as fromPages from "./pages";
import * as fromComponents from "./components";
import * as fromContainers from "./containers";
import * as fromServices from "./services";
import { UILibModule } from 'projects/ui-lib/src/public-api';
import { NgwWowModule } from 'ngx-wow';

@NgModule({
  declarations: [
    AppComponent,
    ...fromPages.pages,
    ...fromComponents.components,
    ...fromContainers.containers
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: "serverApp" }),
    BrowserAnimationsModule,
    AppRoutingModule,
    CarouselModule,
    UILibModule,
    NgwWowModule
  ],
  providers: [
    ...fromServices.services
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

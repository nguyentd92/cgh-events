import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import * as fromPages from "./pages";


const routes: Routes = [
  {
    path: "home",
    component: fromPages.HomeComponent
  },
  {
    path: "about",
    component: fromPages.AboutComponent
  },
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

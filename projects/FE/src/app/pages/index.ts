import { HomeComponent } from './home/home.component';
import { EventComponent } from './event/event.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';

export const pages = [
    HomeComponent,
    EventComponent,
    ContactComponent,
    AboutComponent
]

export * from './home/home.component';
export * from './event/event.component';
export * from './contact/contact.component';
export * from "./about/about.component";
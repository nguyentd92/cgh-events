import { Directive, HostListener } from "@angular/core";

@Directive({
  selector: "[libScrollTop]"
})
export class ScrollTopDirective {
  @HostListener("click", ["$event"]) onClick() {
    window.scroll(0, 0);
  }
}

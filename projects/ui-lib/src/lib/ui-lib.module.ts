import { NgModule } from '@angular/core';
import { UILibComponent } from './ui-lib.component';
import { ScrollTopDirective } from './scroll-top.directive';
import { SortableDirective } from './sortable.directive';
import { ImageControlComponent } from './image-control/image-control.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [UILibComponent, ScrollTopDirective, SortableDirective, ImageControlComponent],
  imports: [
    CommonModule
  ],
  exports: [ScrollTopDirective, SortableDirective, ImageControlComponent]
})
export class UILibModule {}

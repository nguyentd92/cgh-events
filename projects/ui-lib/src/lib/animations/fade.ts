import {
  trigger,
  style,
  query,
  stagger,
  animate,
  transition
} from '@angular/animations';

export const fade = trigger('fade', [
  transition('* => *', [
    // Initially the all cards are not visible
    query(
      ':enter',
      stagger(100, [
        animate(
          '.2s ease-in',
          style({
            opacity: 1
          })
        )
      ]),
      { optional: true }
    ),
    query(':leave', style({ opacity: 1 })),
    query(
      ':leave',
      stagger(100, [
        animate(
          '.2s ease-in',
          style({
            opacity: 0
          })
        )
      ]),
      { optional: true }
    ),
  ])
]);

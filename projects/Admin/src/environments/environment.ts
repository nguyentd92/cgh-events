// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCFbA5K_azYm3aNKeIKcawECXIQ6QD3XB0',
    authDomain: 'cgh-events.firebaseapp.com',
    databaseURL: 'https://cgh-events.firebaseio.com',
    projectId: 'cgh-events',
    storageBucket: 'cgh-events.appspot.com',
    messagingSenderId: '795932328925',
    appId: '1:795932328925:web:d0126b4b8855e254336c34',
    measurementId: 'G-CM2F4VPWEB'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

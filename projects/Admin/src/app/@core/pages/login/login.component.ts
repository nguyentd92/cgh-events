import { Component, OnInit } from '@angular/core';
import { AuthStateService } from '../../services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  error = null;

  constructor(
    public authService: AuthStateService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit() {
    this.formLogin = this.fb.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.minLength(8), Validators.required]]
    });
  }

  get f() {
    return this.formLogin.controls;
  }

  login() {
    if (this.formLogin.invalid) {
      return;
    }

    const { email, password } = this.formLogin.value;
    this.authService.SignIn(email, password).catch(err => {
      this.error = err.message;
      console.log(this.error);
    });
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import * as fromCorePages from './pages';
import * as fromCoreComponents from './components';
import * as fromCoreLayouts from './layouts';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ...fromCorePages.pages,
    ...fromCoreComponents.components,
    ...fromCoreLayouts.layouts
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [...fromCorePages.pages, ...fromCoreLayouts.layouts]
})
export class CoreModule {}

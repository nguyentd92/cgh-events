declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const APP_ROUTE_LIST: RouteInfo[] = [
    { path: '/dashboard', title: 'Dashboard',  icon: 'ni-tv-2 text-primary', class: '' },
    { path: '/events', title: 'Sự kiện', icon: 'ni-paper-diploma text-blue', class: ''}
    // { path: 'icons', title: 'Icons',  icon:'ni-planet text-blue', class: '' },
    // { path: 'maps', title: 'Maps',  icon:'ni-pin-3 text-orange', class: '' },
    // { path: 'user-profile', title: 'User profile',  icon:'ni-single-02 text-yellow', class: '' },
    // { path: 'tables', title: 'Tables',  icon:'ni-bullet-list-67 text-red', class: '' },
    // { path: 'login', title: 'Login',  icon:'ni-key-25 text-info', class: '' },
    // { path: 'register', title: 'Register',  icon:'ni-circle-08 text-pink', class: '' }
];

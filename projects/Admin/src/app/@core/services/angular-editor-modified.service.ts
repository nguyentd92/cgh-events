import { Injectable } from '@angular/core';
import { AngularEditorService, UploadResponse } from '@kolkov/angular-editor';
import { Observable } from 'rxjs';
import { HttpEvent, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AngularEditorModifiedService extends AngularEditorService {
  constructor(httpClient: HttpClient, doc: any){
    super(httpClient, doc);
  }
  // uploadImage(file: File): Observable<HttpEvent<UploadResponse>> {
  //   return of(new HttpEvent<UploadResponse)
  // }
}

import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  UrlSegment,
  Router
} from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { Route } from '@angular/compiler/src/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { tap, map, take } from 'rxjs/operators';

import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class AuthStateService {
  private userDataSubject = new BehaviorSubject<firebase.User>(null);
  private loggingSubject = new BehaviorSubject<boolean>(true);

  private redirectTo: string;

  constructor(
    private angularFireAuth: AngularFireAuth,
    private router: Router
  ) {
    angularFireAuth.authState
      .pipe(
        tap(e => {
          if (!e) {
            this.setRedirectTo(this.router.url);
            this.router.navigateByUrl('/login');
          } else {
            this.router.navigateByUrl('/dashboard');
          }

          this.loggingSubject.next(false);
        })
      )
      .subscribe(u => this.userDataSubject.next(u));
  }

  setRedirectTo(path: string) {
    this.redirectTo = path;
    console.log(this.redirectTo)
  }

  logging$: () => Observable<boolean> = () =>
    this.loggingSubject.asObservable();

  userData$: () => Observable<firebase.User> = () =>
    this.userDataSubject.asObservable();

  /* Just Admin Sign in */
  SignIn(email: string, password: string) {
    this.loggingSubject.next(true);

    return this.angularFireAuth.auth
      .signInWithEmailAndPassword(email, password)
      .finally(() => this.loggingSubject.next(false));
  }

  SignOut() {
    this.angularFireAuth.auth.signOut();
  }

  /** Auth Guards */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.checkLogin$();
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return this.checkLogin$();
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]
  ): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkLogin$();
  }

  private checkLogin$: () => Observable<boolean> = () => {
    // this.setRedirectTo(this.router.url);
    return this.angularFireAuth.authState.pipe(
      take(1),
      map(state => !!state),
      tap(authenticated => {
        console.log('auth', authenticated);
        if (!authenticated) {
          this.router.navigate(['/login']);
        }
      })
    );
  };
}

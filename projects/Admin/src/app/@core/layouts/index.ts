import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './auth-layout/auth-layout.component';

export const layouts = [AdminLayoutComponent, AuthLayoutComponent];

export * from './admin-layout/admin-layout.component';
export * from './auth-layout/auth-layout.component';

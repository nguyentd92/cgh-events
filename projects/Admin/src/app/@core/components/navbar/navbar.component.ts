import { Component, OnInit, ElementRef } from '@angular/core';
import {
  Location,
  LocationStrategy,
  PathLocationStrategy
} from '@angular/common';
import { Router } from '@angular/router';
import { AuthStateService } from '../../services';
import { APP_ROUTE_LIST } from '../../constants/app-menu';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public focus;
  public listTitles: any[];
  public location: Location;
  constructor(
    location: Location,
    private element: ElementRef,
    private router: Router,
    public authService: AuthStateService
  ) {
    this.location = location;
  }

  ngOnInit() {
    this.listTitles = APP_ROUTE_LIST.filter(listTitle => listTitle);
  }
  getTitle() {
    let titlee = this.location.prepareExternalUrl(this.location.path());
    if (titlee.charAt(0) === '#') {
      titlee = titlee.slice(1);
    }

    const item = this.listTitles.find((el, index) => titlee.includes(el.path));

    return item ? item.title : 'Dashboard';
  }
}

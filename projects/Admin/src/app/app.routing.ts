import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import * as fromCore from './@core';

const APP_ROUTES: Routes = [
  /** Basic template from publisher, Remove when release */
  {
    path: 'demo',
    loadChildren: () => import('./demo/demo.module').then(m => m.DemoModule)
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  /** Auth Router */
  {
    path: '',
    component: fromCore.AuthLayoutComponent,
    children: [
      {
        path: 'login',
        component: fromCore.LoginComponent
      }
    ]
  },

  /** Main modules in application - Must be authenticated as admin */
  {
    path: '',
    component: fromCore.AdminLayoutComponent,
    canActivate: [fromCore.AuthStateService],
    canActivateChild: [fromCore.AuthStateService],
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('./modules/dashboard/dashboard.module').then(
            m => m.DashboardModule
          )
      },
      {
        path: 'events',
        loadChildren: () =>
          import('./modules/events/events.module').then(m => m.EventsModule)
      },
      {
        path: 'users',
        loadChildren: () =>
          import('./modules/users/users.module').then(m => m.UsersModule)
      }
    ]
  }
];

@NgModule({
  imports: [CommonModule, BrowserModule, RouterModule.forRoot(APP_ROUTES)],
  exports: []
})
export class AppRoutingModule {}

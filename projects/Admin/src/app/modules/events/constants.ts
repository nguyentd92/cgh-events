import { EventStatus, EVENT_STATUS } from 'projects/data/src/lib/classes/event';

export function getBulletClassByStatus(status: EventStatus): string {
  let btClass = '';
  switch (status) {
    case EVENT_STATUS.PENDING:
      btClass = 'bg-warning';
      break;
    case EVENT_STATUS.ONSCHEDULE:
      btClass = 'bg-info';
      break;
    case EVENT_STATUS.DELAYED:
      btClass = 'bg-danger';
      break;
    case EVENT_STATUS.COMPLETED:
      btClass = 'bg-success';
      break;
  }

  return btClass;
}

export const PAGE_LIMIT = 5;

import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { EventsService } from '../events.service';
import { getBulletClassByStatus } from '../constants';
import { map, tap } from 'rxjs/operators';
import {
  SortableDirective,
  SortEvent,
  compare,
  SortDirection
} from 'projects/ui-lib/src/lib/sortable.directive';

import * as fromUiLib from 'projects/ui-lib/src/lib';
import { CghEvent } from 'projects/data/src/lib/classes/event';
import { Observable } from 'rxjs';
import { EventsOverviewComponent } from '../events-overview/events-overview.component';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.scss']
})
export class EventsListComponent {
  page = 1;

  direction: SortDirection = '';

  @ViewChildren(SortableDirective) headers: QueryList<SortableDirective>;

  constructor(public eventsService: EventsService) {}

  get getEvents$(): Observable<CghEvent[]> {
    return this.eventsService.getEvents$.pipe(
      map(ev => {
        ev.map(e => (e.blClass = getBulletClassByStatus(e.status)));
        return ev;
      })
    );
  }

  selectPage(page: number) {
    this.page = page;
    this.eventsService.loadPagination(page);
  }

  nextPage() {
    this.selectPage(++this.page);
  }

  previousPage() {
    this.selectPage(--this.page);
  }

  onSort({ column = 'name', direction }: SortEvent) {
    this.direction = direction;
    // resetting other headers
    this.headers.forEach(header => {
      if (header.libSortable !== column) {
        header.direction = '';
      }
    });

    // sorting countries
    // if (direction === '') {
    //   this.sortedData = this.rawData;
    // } else {
    //   this.sortedData = [...this.rawData].sort((a, b) => {
    //     const res = compare(a[column], b[column]);
    //     return direction === 'asc' ? res : -res;
    //   });
    // }
  }

  openOverviewDialog() {
    this.eventsService.openDialog(EventsOverviewComponent);
  }
}

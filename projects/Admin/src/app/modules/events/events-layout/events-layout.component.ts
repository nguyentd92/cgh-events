import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EventsOverviewComponent } from '../events-overview/events-overview.component';
import { EventsCreateComponent } from '../events-create/events-create.component';

@Component({
  selector: 'app-events-layout',
  templateUrl: './events-layout.component.html',
  styleUrls: ['./events-layout.component.scss'],
  host: {
    class: 'gradient-header module-header'
  }
})
export class EventsLayoutComponent implements OnInit {
  constructor(private dialog: MatDialog) {}

  ngOnInit() {}

  openCreateDialog() {
    this.dialog.open(EventsCreateComponent, {
      width: '90vw',
      height: '90vh',
      maxWidth: 'none'
    });
  }

  openDialog() {
    this.dialog.open(EventsOverviewComponent, {
      width: '90vw',
      height: '90vh',
      maxWidth: 'none'
    });
  }
}

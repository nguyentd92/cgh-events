import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { EventsLayoutComponent } from './events-layout/events-layout.component';
import { EventsListComponent } from './events-list/events-list.component';
import { EventsCreateComponent } from './events-create/events-create.component';
import { EventsService } from './events.service';

import { MatDialogModule, MatStepperModule } from '@angular/material';
import { MatTabsModule } from '@angular/material';
import { AngularEditorModule } from '@kolkov/angular-editor';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { UILibModule } from 'projects/ui-lib/src/public-api';
import { EventsOverviewComponent } from './events-overview/events-overview.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const EVENT_ROUTES: Routes = [
  {
    path: '',
    component: EventsLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
      },
      {
        path: 'list',
        component: EventsListComponent
      }
    ]
  }
];

@NgModule({
  declarations: [
    EventsLayoutComponent,
    EventsListComponent,
    EventsCreateComponent,
    EventsOverviewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    UILibModule,
    MatTabsModule,
    MatStepperModule,
    MatDialogModule,
    AngularEditorModule,
    RouterModule.forChild(EVENT_ROUTES)
  ],
  providers: [EventsService],
  entryComponents: [EventsCreateComponent, EventsOverviewComponent]
})
export class EventsModule {}

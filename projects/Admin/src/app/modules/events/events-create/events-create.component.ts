import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { NgbTimepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EventsService } from '../events.service';
import { AngularFireStorage } from '@angular/fire/storage';

@Component({
  selector: 'app-events-create',
  templateUrl: './events-create.component.html',
  styleUrls: ['./events-create.component.scss']
})
export class EventsCreateComponent implements OnInit {
  htmlContent: string;

  formInfo: FormGroup;

  time = { hour: 13, minute: 30 };

  profileUrl = '';

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '400px',
    maxHeight: '100%',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Enter text here...',
    defaultParagraphSeparator: '',
    defaultFontName: '',
    defaultFontSize: '',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' }
    ],
    customClasses: [
      {
        name: 'quote',
        class: 'quote'
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1'
      }
    ],
    uploadUrl: 'v1/image',
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [['bold', 'italic'], ['fontSize']]
  };

  constructor(
    config: NgbTimepickerConfig,
    private formBuilder: FormBuilder,
    private eventService: EventsService,
    private storage: AngularFireStorage
  ) {
    config.spinners = false;
  }

  ngOnInit() {
    const ref = this.storage.ref('logos/FFD6F8F7-A15A-43FE-9EC0-C6DE1BE77452.jpeg');
    ref.getDownloadURL().subscribe(e => console.log('Image Url', e));

    this.formInfo = this.formBuilder.group({
      title: ['', [Validators.required]],
      maxVolume: [200, [Validators.required]],
      description: ['', []],
      logo: [''],
      address: [''],
      city: [''],
      province: [''],
      startDate: [''],
      startTime: [''],
      endDate: [''],
      endTime: ['']
    });
  }

  submit() {
    this.eventService.createEvent(this.formInfo.value).then();
  }

  changeLogo(event) {
    this.formInfo.patchValue({
      logo: event
    });

    this.eventService.uploadFile(event);
  }
}

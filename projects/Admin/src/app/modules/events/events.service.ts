import { Injectable, PipeTransform } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { CghEvent, EVENTS } from 'projects/data/src/lib/classes/event';
import { MatDialog } from '@angular/material';

import {
  AngularFirestore
} from '@angular/fire/firestore';

import { AngularFireStorage } from '@angular/fire/storage';

const ENDPOINT = 'events/';

@Injectable()
export class EventsService {
  private eventSubject = new BehaviorSubject<CghEvent[]>([]);

  constructor(private dialog: MatDialog, private firestore: AngularFirestore, private afStorage: AngularFireStorage) {
    this.firestore
      .collection(ENDPOINT)
      .snapshotChanges()
      .subscribe(l =>
        this.eventSubject.next(
          l.map(e => ({
            id: e.payload.doc.id,
            ...e.payload.doc.data()
          } as CghEvent))
        )
      );
  }

  uploadFile(file) {
    this.afStorage.upload('/logos/' + file.name, file).then(e => console.log(e));
  }

  getEvent() {}

  createEvent(event: CghEvent) {
    return this.firestore.collection(ENDPOINT).add(event);
  }

  updateEvent(event: CghEvent) {
    delete event.id;
    this.firestore.doc(ENDPOINT + event.id).update(event);
  }

  deleteEvent(id: string) {
    this.firestore.doc(ENDPOINT + id).delete();
  }

  get getEvents$(): Observable<CghEvent[]> {
    return this.eventSubject.asObservable();
  }

  openDialog(component, data?: any) {
    this.dialog.open(component, {
      width: '90vw',
      height: '90vh',
      data
    });
  }

  loadAllEvents = () => this.eventSubject.next(EVENTS);

  searchEvents(keyword: string, pipe: PipeTransform) {
    // this.eventSubject.next(
    //   EVENTS.filter(event => {
    //     const term = keyword.toLowerCase();
    //     return (
    //       event.name.toLowerCase().includes(term) ||
    //       pipe.transform(event.area).includes(term) ||
    //       pipe.transform(event.population).includes(term)
    //     );
    //   })
    // );
  }

  loadPagination(page: number = 1, limit: number = 5) {
    const startIndex = (page - 1) * limit;
    const endIndex = startIndex + limit;

    this.eventSubject.next(EVENTS.slice(startIndex, endIndex));
  }
}
